<!DOCTYPE html>
<html>
<head>
    <title>My PHP Web Server</title>
</head>
<body>
    <h1>Welcome to my PHP Web Server!</h1>
    <?php
        // Выводим информацию о PHP
        echo "<h2>PHP Information</h2>";
        echo "PHP Version: " . PHP_VERSION . "<br>";
        echo "Server Name: " . $_SERVER['SERVER_NAME'] . "<br>";
        echo "Server Software: " . $_SERVER['SERVER_SOFTWARE'] . "<br>";

        // Выводим информацию о текущей дате и времени
        echo "<h2>Current Date and Time</h2>";
        echo "Current Date: " . date("Y-m-d") . "<br>";
        echo "Current Time: " . date("H:i:s") . "<br>";
    ?>
</body>
</html>